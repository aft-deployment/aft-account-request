# Copyright Amazon.com, Inc. or its affiliates. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
#
module "sandbox_account_test_05" {
  source = "./modules/aft-account-request"

  control_tower_parameters = {
    AccountEmail = "us_aws-control-tower-accelerator+AFT-Account_custom0005@pwc.com"
    AccountName  = "sandbox_account_test_05"
    # Syntax for top-level OU
    ManagedOrganizationalUnit = "Sandbox"
    # Syntax for nested OU
    # ManagedOrganizationalUnit = "Sandbox (ou-xfe5-a8hb8ml8)"
    SSOUserEmail     = "judith.soundarya.joseph@pwc.com"
    SSOUserFirstName = "Judith"
    SSOUserLastName  = "J"
  }

  account_tags = {
    "Owner"       = "judith.soundarya.joseph@pwc.com"
  }

   change_management_parameters = {
    change_requested_by = "judith"
    change_reason       = "testing the account vending process"
  }



  account_customizations_name = "sandbox02"
}
